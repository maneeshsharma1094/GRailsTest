//
//  HomeViewController.swift
//  FirebaseTutorial
//
//  Created by James Dacombe on 16/11/2016.
//  Copyright © 2016 AppCoda. All rights reserved.
//

import UIKit
import Photos
import Firebase
import BSImagePicker





class HomeViewController: UIViewController {
    @IBOutlet weak var ageBox: UITextField!


    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var facialProfileBox: UITextField!
    @IBOutlet weak var nameBox: UITextField!
    var images = [UIImage]()
    var imageAssets = [PHAsset]()
    var imageDownloadURL = String()
//    let imagePicker = UIImagePickerController()
    var userMail = String()
    let listOfDiseases = ["None", "Alzheimer's disease", "Cancer", "Influenza", "Pneumonia", "Chronic Respiratory issues"]
    @IBOutlet weak var medicalHistoryTextView: UITextField!
    var selectedImage = UIImage()
    var ref: DatabaseReference!
    let imagePicker = ImagePickerController()
    
    typealias FileCompletionBlock = () -> Void
    var block: FileCompletionBlock?


    
    @IBOutlet weak var titleLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        medicalHistoryTextView.loadDropdownData(data: listOfDiseases)
        titleLabel.layer.borderWidth = 3.0
        titleLabel.layer.borderColor = UIColor.white.cgColor
        ref = Database.database().reference()
        let uid  = Auth.auth().currentUser?.uid


        ref.child("Notifications").child("t3lanyWN1XDw4n8Vgkpd").observe(.value, with: {snapshot in
            if let value = snapshot.value as? [String : AnyObject] {
                let username = value["Exit"] as? String ?? ""
            }
              // ...
              }) { (error) in
                print(error.localizedDescription)
            }
        let db = Firestore.firestore()

        db.collection("Notifications").document("t3lanyWN1XDw4n8Vgkpd")
        .addSnapshotListener { documentSnapshot, error in
          guard let document = documentSnapshot else {
            print("Error fetching document: \(error!)")
            return
          }
          guard let data = document.data() else {
            print("Document data was empty.")
            return
          }
          print("Current data: \(data)")
            if let notificationArray : [String] = data["Notification"] as? [String] {

            for notifContent in notificationArray {
                let content = UNMutableNotificationContent()
                          content.title = "Covid-19 Safety"
                          content.body = notifContent
                          let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 2, repeats: false)
                let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
                             UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
                }}

        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func browseButtonClicked(_ sender: Any) {
        
        let alert = UIAlertController(title: "Hello", message: "Please add 5 images for us to train our model with your facial profile", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
              switch action.style{
              case .default:
                self.presentImagePicker(self.imagePicker, select: { (asset) in
                    // User selected an asset. Do something with it. Perhaps begin processing/upload?
                }, deselect: { (asset) in
                    // User deselected an asset. Cancel whatever you did when asset was selected.
                }, cancel: { (assets) in
                    // User canceled selection.
                }, finish: { (assets) in
                    // User finished selection assets.
                    self.imageAssets = assets
                    var i = 0
                    while(i < 5)     {
                        let manager = PHImageManager.default()
                        let options = PHImageRequestOptions()
                        options.deliveryMode = .highQualityFormat




                        PHImageManager.default().requestImage(for: self.imageAssets[i], targetSize: PHImageManagerMaximumSize, contentMode: .aspectFit, options: nil) { (image, info) in
                        self.images.append(image!)
                            i = i+1


                    }

                    }

                    self.uploadImage()
                           })
                
          
                
                
                
                
                break

              case .cancel:
                    print("cancel")

              case .destructive:
                    print("destructive")


        }}))
        self.present(alert, animated: true, completion: nil)

        
        
        
    }
    
    
    
    @IBAction func createButtonClicked(_ sender: Any) {
        
        
        let db = Firestore.firestore()
        
        
        let user = Auth.auth().currentUser
        if let user = user {
          // The user's ID, unique to the Firebase project.
          // Do NOT use this value to authenticate with your backend server,
          // if you have one. Use getTokenWithCompletion:completion: instead.
            userMail = user.email!
          // ...
        }
        
     
        
        var ref: DocumentReference? = nil
        var adminString = Bool()

        if self.medicalHistoryTextView.text == "None" {
        adminString = true
        }
        else {
        adminString = false

        }
        
        ref = db.collection("users").addDocument(data: [
            "name": self.nameBox.text,
            "age": self.ageBox.text,
            "medical history": self.medicalHistoryTextView.text,
            "profilepictureURL": self.imageDownloadURL,
            "entrynotification": ["wash hands", "wipe groceries"],
            "exitnotification": ["wear a mask","social distance"],
            "admin" : adminString,
            "email": userMail
        ]) { err in
            if let err = err {
                print("Error adding document: \(err)")
            } else {
                print("Document added with ID: \(ref!.documentID)")
                let defaults = UserDefaults.standard
                defaults.set(ref!.documentID, forKey: "userID")


            }
        }
        
        
        db.collection("Profiles").document("vBelq0WbFAlh5fKeDuiy").setData([
            self.nameBox.text! : "5"

        ], merge: true) { err in
            if let err = err {
                print("Error writing document: \(err)")
            } else {
                print("Document successfully written!")
            }
        }
        
    }
    
    func uploadImage() {
        var i = 0
        while(i < 5) {
            print("The images count is \(images.count)")
            print("The index is \(i)")
           
            PostService().create(for: images[i], name: "\((Auth.auth().currentUser!.displayName)!)/\(UUID().uuidString).jpg") { (downloadURL) in

                guard let downloadURL = downloadURL else {
                               return
                           }
                self.imageDownloadURL = downloadURL
                print("image url in Completion Handler is : \(self.imageDownloadURL)")
            }
            
            i = i + 1

            }


        /// All images have been uploaded successfully
    
        return;
    }    
    
    @IBAction func nameFinishedEditing(_ sender: UITextField!) {
        
        let user = Auth.auth().currentUser

                   let changeRequest = user!.createProfileChangeRequest()
                        changeRequest.displayName = self.nameBox.text!
                        changeRequest.commitChanges { (error) in
                            print("Error while adding user name - \(error)")
                        }
    }
    

    
    func delay(_ delay:Double, closure:@escaping ()->()) {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }
 

    
    
}




extension UIButton {

/// Add image on left view
func leftImage(image: UIImage) {
  self.setImage(image, for: .normal)
  self.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: image.size.width)
}
}
